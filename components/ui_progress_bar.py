from ursina import *

class UIProgressBar():

    def __init__(
            self,
            position: tuple = (0, -0.025),
            body_color: color = color.green,
            frame_color=color.white,
            parent = None,
            below: Entity = None,
            max_value: int = 10,
            width_scale: int = 1,
            ):

        if parent is None:
            raise Exception("please set the ProgressBar parent to camera.ui")

        self.max_value = max_value
        self.scale_x_factor = 0.1 * width_scale
        self.scale_y_factor = 0.01
        origin = (-0.5, 0.5)

        self._frame: Entity = Entity(
            model='quad',
            color=frame_color,
            scale_x=self.scale_x_factor,
            scale_y=self.scale_y_factor,
            scale_max=1,
            z=0,
            position = position,
            origin=origin,
            parent=parent,
        )

        self._bar = Sprite(
            origin=origin,
            z=-0.01,
            scale_x=self.scale_x_factor,
            scale_y=self.scale_y_factor,
            scale_max=1,
            color=body_color,
            position=position,
            parent=parent,
        )

        if below:
            width = self._frame.scale_x
            self._bar.position = (below.x - width + position[0], below.y + position[1])
            self._frame.position = (below.x - width + position[0], below.y + position[1])

    def set_progress(self, value) -> None:
        value = max(0, min(value, self.max_value))
        percentage = value / self.max_value

        self._bar.scale_x = percentage * self.scale_x_factor
