from ursina import *

class ProgressBar():

    def __init__(
            self,
            title: str = "PLACEHOLDER",
            origin: tuple = (-0.5, 0.5),
            position: tuple = (0, 1),
            body_color: color = color.green,
            max_value: int = 10,
            frame_color=color.white,
            ):

        self.max_value = max_value

        self._frame: Entity = Entity(
            model='quad',
            color=frame_color,
            scale_x=1,
            scale_y=0.1,
            scale_max=max_value,
            x = position[0],
            y = position[1],
            origin=origin,
        )

        self._bar: Entity = Entity(
            model='quad',
            color=body_color,
            scale_x=1,
            scale_y=0.1,
            scale_max=1,
            origin=origin,
            x = position[0],
            y = position[1],
            z=-0.01,
        )
    """
        self._label: Text = Text(
            text=title,
            origin=origin,
            scale_x=10,
            scale_y=3,
            z =-0.2,
            color=color.black,
            parent=scene,
        )
        self._label.x = position[0] - self._label.width/2
        self._label.y = position[1] + self._label.height/2
    """

    def set_progress(self, value) -> None:
        value = max(0, min(value, self.max_value))

        scale_x = value / self.max_value

        self._bar.scale_x = scale_x
