from ursina import Button
import random

class DangerManager():
    def __init__(self, button: Button):
        self.button = button
        self._allowed = True
        self._enabled = False
        self.timestamp_last_pressed = 0
        self.timestamp_check = 0

        self.button.enabled = self._enabled

    @property
    def enabled(self):
        return self._allowed

    @enabled.setter
    def enabled(self, val: bool):
        self._allowed = val
        self.button.enabled = val
        if val and self._enabled:
            self._shake_it()

    def tick(self, time_elapsed: int):
        check_interval = 1

        if self._enabled:
            if self._allowed:
                self.button.enabled = True
            return
        if self.timestamp_check + check_interval > time_elapsed:
            return
        self.timestamp_check = time_elapsed
        max_time = 12
        diff = time_elapsed - self.timestamp_last_pressed
        rand = random.random()

        if (diff / max_time) > rand:
            self._enabled = True
            self._shake_it()

    def _shake_it(self):
        self.button.shake(duration=1, magnitude=0.2)

    def pressed(self, time_elapsed: int):
        self._enabled = False
        self.button.enabled = False
        self.timestamp_last_pressed = time_elapsed
