#!/usr/bin/env python3

from ursina import scene


class ViewNavigator:
    def __init__(self, views, left_button=None, right_button=None):
        assert(isinstance(views, list))
        assert(len(views) > 0)

        self.left_button = left_button
        if left_button is not None:
            left_button.on_click = self.navigate_left

        self.right_button = right_button
        if right_button is not None:
            right_button.on_click = self.navigate_right

        self.views = views

    def navigate_left(self):
        new_view = self.views[-1]

        scene.camera.x = new_view['position']
        if self.left_button is not None:
            self.left_button.text = new_view['left text']
        if self.right_button is not None:
            self.right_button.text = new_view['right text']

        for b in new_view['buttons']:
            b.visible = True
            b.enabled = True
        for v in self.views[:-1]:
            for b in v['buttons']:
                b.visible = False
                b.enabled = False

        self.views = [new_view] + self.views[:-1]

    def navigate_right(self):
        new_view = self.views[1]

        scene.camera.x = new_view['position']
        if self.left_button is not None:
            self.left_button.text = new_view['left text']
        if self.right_button is not None:
            self.right_button.text = new_view['right text']

        for b in new_view['buttons']:
            b.visible = True
            b.enabled = True
        for v in self.views[2:]:
            for b in v['buttons']:
                b.visible = False
                b.enabled = False
        for b in self.views[0]:
            b.visible = False
            b.enabled = False

        self.views = self.views[1:] + [self.views[0]]
