# Title: In espace no one can hear you escream
## Theme: Creation and Destruction

Henrik, Gustavo, Anders, Maibritt, Søren, Luna, Tjalf

April 2024

# Visuals 
- Inner spaceship area
    - Kitchen area (fridge and deep fryer)
    - Lab area (spectrometry and penguins)
    - Storage and supply area
        - Oli tank
        - Water
        - Electricity generator
        - Toolbox
- Computer screen
    - With overview of Vitals
    - Computer can run out of electricty 
    - Radar (for aliens, spaceships)
- Window with view of the outside
    - Aliens
    - Other spaceshipts (friends and enemies)
    - 
