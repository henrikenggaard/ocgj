from ursina import *
from spaceship import Engine, DeepFryer, OilTank, Battery, Watertank, Fridge, Aquarium, Penguin
from partwrapper import PartWrapper
import value_metrics
from orbitmanager import OrbitManager

class Score(Entity):
    def __init__(self, parts: "dict[PartWrapper]", background_entity: Entity, orbit_manager: OrbitManager):
        self.is_running: bool = True
        self.fail_message: str = None
        self.dialog_spawned: bool = False
        self.won_the_game: bool = False
        self.parts: PartWrapper = parts
        self.background: Entity = background_entity
        self.orbit_manager = orbit_manager

        crew_hunger: int
        crew_sanity: int
        crew_health: int
        difficulty: int

        self.time_elapsed: int = 0
        self.game_time: int = 120

        self.engine: Engine = self.parts["engine"]._part
        if not isinstance(self.engine, Engine):
            raise Exception("Score needs an engine part to function")

        self.deep_fryer: DeepFryer = self.parts['deep_fryer']._part
        if not isinstance(self.deep_fryer, DeepFryer):
            raise Exception("Score needs a deep fryer to function")

        self.oil_tank: OilTank = self.parts['oil_tank']._part
        if not isinstance(self.oil_tank, OilTank):
            raise Exception("Score needs an oil tank to function")

        self.battery: Battery = self.parts['battery']._part
        if not isinstance(self.battery, Battery):
            raise Exception("Score needs a battery to function")

        self.watertank: Watertank = self.parts['watertank']._part

        self.fridge: Fridge = self.parts['fridge']._part

        self.aquarium: Aquarium = self.parts['aquarium']._part

        self.penguin: Penguin = self.parts['penguin']._part

    def update(self):
        self.check_if_engine_dead()
        self.check_if_battery_empty()
        self.check_food_supply()
        self.check_if_penguin_is_alive()
        self.check_if_time_is_up()

        if not self.is_running:
            if not self.dialog_spawned:
                if self.won_the_game:
                    self.won_dialog_box()
                    Audio("assets/game_won.mp3", autoplay=False).play()
                else:
                    self.failed_dialog_box()
                    Audio("assets/game_over.mp3", autoplay=False).play()
                self.dialog_spawned = True

        # Update time elapsed
        self.time_elapsed += time.dt

    def check_food_supply(self):
        if self.deep_fryer.holding['food'] < 0:
            self.is_running = False
            self.fail_message = "Your food supplies ran out!"

    def check_if_penguin_is_alive(self):
        if self.penguin.holding['food'] <= 0:
            self.is_running = False
            self.fail_message = "Your penguin died!"

    def check_if_engine_dead(self):
        if self.engine.holding['health'] <= 0:
            self.is_running = False
            self.fail_message = "Your engine died!"

    def check_if_battery_empty(self):
        if self.battery.holding['energy'] <= 0:
            self.is_running = False
            self.fail_message = "You ran out of battery!"

    def check_if_time_is_up(self):
        if self.time_elapsed >= self.game_time:
            self.is_running = False
            self.won_the_game = True

    def get_score(self) -> int:
        _engine_health_score_factor = 0.4
        _food_score_factor = 0.1
        _time_score_factor = 1

        return int(
            (self.time_elapsed * _time_score_factor)
            * (self.engine.holding['health'] * _engine_health_score_factor +
               self.deep_fryer.holding['food'] * _food_score_factor))

    def failed_dialog_box(self):
        # Change Background
        self.safe_background_texture = self.background.texture
        self.background.texture = "assets/game_over/game_over.png"

        self.safe_background_z = self.background.z
        self.background.z = -0.1

        # Dialog
        btn_accept = Button(text='Yeah I suck, let me try again!')
        btn_quit = Button(text='I Quit!')

        wp = WindowPanel(
            title='Game Over',
            content=(
                Text('You lost the game', origin=(0.5,0.5)),
                Text(self.fail_message),
                Text(f'Score: {self.get_score()}'),
                btn_accept,
                btn_quit,
                ),
            popup=False
            )
        wp.y = wp.panel.scale_y / 2 * wp.scale_y    # center the window panel
        wp.layout()

        def accept():
            print(f"Go again!")
            self.recover_game()
            wp.close()
        def quit():
            print(f"quit the game!")
            application.quit()

        btn_accept.on_click = accept
        btn_quit.on_click = quit

    def won_dialog_box(self):
        # Change Background
        self.safe_background_texture = self.background.texture
        self.background.texture = "assets/end_success.png"

        self.safe_background_z = self.background.z
        self.background.z = -0.1

        # Dialog
        btn_accept = Button(text='Hurray! let me try again!')
        btn_quit = Button(text='That was nice, bye!')

        wp = WindowPanel(
            title='Game Over',
            content=(
                Text('You won!', origin=(0.5,0.5)),
                Text(f'Score: {self.get_score()}'),
                btn_accept,
                btn_quit,
                ),
            popup=False
            )
        wp.y = wp.panel.scale_y / 2 * wp.scale_y    # center the window panel
        wp.layout()

        def accept():
            print(f"Go again!")
            self.recover_game()
            wp.close()
        def quit():
            print(f"quit the game!")
            application.quit()

        btn_accept.on_click = accept
        btn_quit.on_click = quit


    def recover_game(self):
        # Recover Background
        self.background.texture = self.safe_background_texture
        self.background.z = self.safe_background_z

        # Reset parts values
        self.engine.holding['health'] = value_metrics.health_start
        self.engine.broken = False
        self.deep_fryer.holding['food'] = value_metrics.food_start
        self.deep_fryer.broken = False
        self.oil_tank.holding['oil'] = value_metrics.oil_start
        self.oil_tank.broken = False
        self.battery.holding["energy"] = value_metrics.energy_start
        self.battery.broken = False
        self.watertank.holding["water"] = 100
        self.watertank.broken = False
        self.fridge.holding["ice"] = 100
        self.fridge.broken = False
        self.aquarium.holding["fish"] = 100
        self.aquarium.broken = False
        self.penguin.holding["health"] = 100
        self.penguin.broken = False


        # Reset orbits
        self.orbit_manager.reset()

        # Game logic
        self.won_the_game = False
        self.is_running = True
        self.dialog_spawned = False
        self.time_elapsed = 0



