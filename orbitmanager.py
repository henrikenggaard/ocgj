#!/usr/bin/env python3

from math import copysign
from random import choice, uniform

from ursina import Sprite, curve, destroy

from spaceship import SpaceshipPart

ORBIT_X_OFFSET = -15

class Orbit:
    def __init__(self, background, y):
        self.background = background
        self.y = y
        self.y_abs = y
        self.entities = []

    def update_y(self, new_y):
        self.background.y = new_y
        ydiff = new_y - self.y
        self.y = new_y
        for ent in self.entities:
            ent.update_y(ydiff)

    def origo(self):
        return (self.y, ORBIT_X_OFFSET)

class Asteroid(Sprite):
    def __init__(self, orbit, start, end, duration, texture):
        self.orbit = orbit

        origo = self.orbit.origo()
        super().__init__(texture=texture,
                         x=origo[1] - 10,
                         y=origo[0] + start,
                         scale=0.3,
                         z=uniform(0.01, 0.1),
                         collider='sphere',
                         rotation_z=uniform(0, 360))
        self.animate('x', -5, duration=duration, curve=curve.linear)

    def on_click(self):
        self.orbit.add_water()
        self.x = 1e6

    def update_y(self, ydiff):
        self.y += ydiff

class AsteroidOrbit(Orbit):
    def __init__(self, asteroid_textures, *args, **kwargs):
        self.asteroid_textures = asteroid_textures
        super().__init__(*args, **kwargs)

    def add_water(self):
        if self.water_target is not None:
            holding = self.water_target.holding
            holding['water'] = max(min(holding.get('water', 0) + 5, 100),
                                       holding.get('water', 0))

    def set_water_target(self, other: SpaceshipPart):
        self.water_target = other

    def update(self):
        def judge(ent):
            if ent.x > ORBIT_X_OFFSET + 7:
                destroy(ent)
                return False
            else:
                return True

        self.entities = [ent for ent in self.entities
                         if judge(ent)]

        for _ in range(7 - len(self.entities)):
            start = uniform(-5, 5)
            end = uniform(-5, 5)
            duration = uniform(6, 20)
            texture = choice(self.asteroid_textures)
            self.entities.append(Asteroid(self, start, end, duration,
                                          texture))

class OrbitManager(SpaceshipPart):
    needs = ['thrust']
    def __init__(self, orbits):
        super().__init__()
        self.orbits = orbits
        self.velocity = 0
        self.target_orbit = None
        self.position = 0

        for k, v in orbits.items():
            if v.y == 0:
                self.current_orbit = k

    def goto_orbit(self, target_orbit):
        assert(target_orbit in self.orbits)
        self.target_orbit = target_orbit
        self.current_orbit = None

    def step(self):
        if self.target_orbit is not None:
            super().step()

    def reset(self):
        self.velocity = 0
        self.target_orbit = None
        self.position = 0
        for k, v in self.orbits.items():
            if v.y == 0:
                self.current_orbit = k

        for k in self.orbits:
            self.orbits[k].update_y(self.orbits[k].y_abs - self.position)

    def update(self):
        if self.target_orbit is not None:
            remainder = self.orbits[self.target_orbit].y_abs - self.position
            velocity_target = 0.01 * copysign(abs(remainder)**0.5, remainder)

            if abs(remainder) < 0.002 and self.velocity < 0.0002:
                self.current_orbit = self.target_orbit
                self.target_orbit = None
                self.velocity = 0
                self.position += remainder
            else:
                if self.has_all_resources:
                    if abs(velocity_target - self.velocity) < 0.0001:
                        self.velocity = velocity_target
                    elif self.velocity < velocity_target:
                        self.velocity += 0.0001
                    else:
                        self.velocity -= 0.0001

                else:
                    if abs(self.velocity) < 0.0001:
                        self.velocity = 0
                    else:
                        self.velocity *= 0.999

                self.position += self.velocity

            for k in self.orbits:
                    self.orbits[k].update_y(self.orbits[k].y_abs - self.position)
