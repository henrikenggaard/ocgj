#!/usr/bin/env python3

from ursina import Audio

class MusicManager:
    def __init__(self):
        self.friendly_theme = Audio("assets/friendly_orbit.mp3", loop=True)
        self.friendly_theme.play()
        self.friendly_theme.volume = 1.0

        self.space_theme = Audio("assets/spaceship.mp3", loop=True)
        self.space_theme.pause()
        self.space_theme.volume = 0

        self.alien_theme = Audio("assets/alien.mp3", loop=True)
        self.alien_theme.pause()
        self.alien_theme.volume = 0

        self.asteroids_theme = Audio("assets/asteroid_.mp3", loop=True)
        self.asteroids_theme.pause()
        self.asteroids_theme.volume = 0

    def _update_orbit(self, orbit_height, target, theme):
        distance = abs(orbit_height - target)
        if distance < 7:
            theme.volume = max((1 - distance / 6) ** 2, 0)
            if not theme.playing:
                theme.resume()
        else:
            theme.volume = 0
            if theme.playing:
                theme.pause()

    def update(self, orbit_height):
        self._update_orbit(orbit_height, 0, self.friendly_theme)
        self._update_orbit(orbit_height, 10, self.space_theme)
        self._update_orbit(orbit_height, 20, self.alien_theme)
        self._update_orbit(orbit_height, 30, self.asteroids_theme)

