import value_metrics

class SpaceshipPart:
    texture = "assets/snoevsen.png"

    metric = "health"

    needs: str = []
    gives: str = []

    needy = False

    broken = False

    needy_loss = 1

    def __init__(self) -> None:
        self.inputs: list[SpaceshipPart] = []
        self.output: SpaceshipPart = None

        self.holding = {g: 0 for g in self.gives}
        self.holding["health"] = value_metrics.health_start
        self.holding["food"] = value_metrics.food_start

        self.has_all_resources = False

    def couple(self, other: "SpaceshipPart"):
        other.decouple()
        self.inputs.append(other)
        other.output = self

    def decouple(self):
        if self.output:
            try:
                self.output.inputs.remove(self)
            except ValueError:
                pass
            self.output.check_resources()

    def check_resources(self):
        resources_that_are_given = set()

        for i in self.inputs:
            if i.broken:
                continue

            available_resources = [k for k, v in i.holding.items() if v > 0]

            resources_that_are_given.update(available_resources)

        self.has_all_resources = all(map(lambda x: x in resources_that_are_given, self.needs))

    def fix(self):
        if self.metric not in ["oil"]:
            self.broken = False
            return

        if self.holding[self.metric] < 100:
            self.holding[self.metric] += 1
            self.broken = False

    def step(self):
        if self.broken:
            return

        self.check_resources()

        if not self.has_all_resources:
            if self.needy:
                self.holding[self.metric] -= self.needy_loss
                if self.holding[self.metric] <= 0:
                    self.broken = True
            return

        for i in self.inputs:
            for n in self.needs:
                if n in i.gives and i.holding[n] > 0:
                    i.holding[n] -= 1

        for g in self.gives:
            self.holding[g] += 1

    def __repr__(self) -> str:
        return self.__class__.__name__ + ": " + str(self.holding)


class OilTank(SpaceshipPart):
    texture = "assets/oil_tank.png"
    gives = ["oil"]

    metric = "oil"

    def __init__(self) -> None:
        super().__init__()

        self.holding["oil"] = 100

    def step(self):
        pass

    def couple(self, other: SpaceshipPart):
        pass


class DeepFryer(SpaceshipPart):
    texture = "assets/deep-fryer.png"
    needs = ["oil", "energy", "fish"]
    gives = ["food"]


class Engine(SpaceshipPart):
    texture = "assets/engine.png"
    needs = ["oil", "energy"]
    gives = ["thrust"]

    metric = "thrust"


class Battery(SpaceshipPart):
    texture = "assets/battery.png"
    gives = ["energy"]

    metric = "energy"

    def __init__(self) -> None:
        super().__init__()

        self.holding["energy"] = 100

    def step(self):
        pass

    def couple(self, other: SpaceshipPart):
        pass


class Watertank(SpaceshipPart):
    texture = "assets/water_tank.png"
    gives = ["water"]

    metric = "water"

    def __init__(self) -> None:
        super().__init__()

        self.holding["water"] = 100

    def step(self):
        pass

    def couple(self, other: SpaceshipPart):
        pass


class Fridge(SpaceshipPart):
    texture = "assets/fridge.png"
    needs = ["water", "energy"]
    gives = ["ice"]

    metric = "ice"

    needy = True
    needy_loss = 0.5

    def __init__(self) -> None:
        super().__init__()

        self.holding["ice"] = 100


class Aquarium(SpaceshipPart):
    texture = "assets/aquarium.png"
    needs = ["water"]
    gives = ["fish"]

    metric = "fish"

    needy = True
    needy_loss = 0.1

    def __init__(self) -> None:
        super().__init__()

        self.holding["fish"] = 100

    def couple(self, other: SpaceshipPart):
        pass


class Penguin(SpaceshipPart):
    texture = "assets/penguin.png"
    needs = ["food", "ice"]

    needy = True
    needy_loss = 0.25


if __name__ == "__main__":
    o = OilTank()
    b = Battery()
    e = Engine()

    e.couple(o)
    e.couple(b)

    e.step()

    print(e)

    o.decouple()

    e.step()

    print(e)
