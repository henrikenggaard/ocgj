#!/usr/bin/env python3

# import pathlib
import random 

from ursina import *

from spaceship import SpaceshipPart, DeepFryer, Engine, OilTank, Battery, Fridge,Aquarium,Watertank, Penguin
from viewnavigator import ViewNavigator
from partwrapper import PartWrapper, PARTS
from components.ui_progress_bar import UIProgressBar
from orbitmanager import OrbitManager, Orbit, AsteroidOrbit
from musicmanager import MusicManager
from danger_manager import DangerManager
from score import Score
import value_metrics

app = Ursina(
    forced_aspect_ratio=True,
    size=(1920,1080),
    borderless=False,
)

music_man = MusicManager()


wow = Audio("assets/wow.mp3", autoplay=False)

def check_window(oil_tank, deep_fryer):
    check_danger.pressed(score.time_elapsed)
    wow.play()
    # Specify the path to the folder
    folder_path = Path("assets/the surprise/")

    # List all files in the folder
    file_list = [str(file) for file in folder_path.iterdir() if file.is_file()]
    random_visit = random.randint(0, len(file_list) - 1)
    file = file_list[random_visit]
    if 'evil' in file and 'oil' in file:
        if oil_tank._part.holding['oil'] > 50:
            oil_tank._part.holding['oil'] -= 50
        else:
            oil_tank._part.holding['oil'] = 0
    elif 'friendly' in  file and 'oil' in file:
        oil_tank._part.holding['oil'] += 50
    elif 'friendly' in file and 'food' in file:
        deep_fryer._part.holding['food'] +=50

    # Create an Entity to display the PNG image
    image = Entity(model='quad', texture=file, scale=(8.0, 8.0), position=(0, 0, -1))
    image.render = 1

    destroy(image, delay=1.5)

dummy = Entity(model='quad', scale=0.001, x=-1e6)

background = Sprite(texture=Texture('assets/inside_spaceship_foreground.png'), z=1)
visitor = Sprite(texture=Texture('assets/astronaut_transparent.png'), z=1.01, scale=0.075, x=-3.5, y=-5)
visitor.animate('y', 5, duration=20, loop=True, curve=curve.linear)
visitor.animate('x', -.25, duration=20, loop=True, curve=curve.linear)
visitor.animate('rotation_z', 96, duration=20, loop=True, curve=curve.linear)
backestground = Sprite(texture=Texture('assets/inside_spaceship_background.png'), z=1.02)

outside = Sprite(texture=Texture('assets/spaceship.png'), x=-11.9, z=1, scale=0.8)
low_earth_orbit = Sprite(texture=Texture('assets/low_earth_orbit.png'), x=-15, z=1.1)
sattelite_orbit = Sprite(texture=Texture('assets/sattelite_orbit.png'), x=-15, y=10, z=1.01)
moon_orbit = Sprite(texture=Texture('assets/moon_orbit.png'), x=-15, y=20, z=1.01)
asteroids_orbit = Sprite(texture=Texture('assets/asteroid_orbit.png'), x=-15, y=30, z=1.01)

asteroid_orbit_obj = AsteroidOrbit([Texture('assets/lone_asteroid_1.png'),
                                    Texture('assets/lone_asteroid_2.png'),
                                    Texture('assets/lone_asteroid_3_256x250px.png'),
                                    ],
                                   asteroids_orbit, 30)

orbman = OrbitManager({'leo': Orbit(low_earth_orbit, 0),
                       'sattelite': Orbit(sattelite_orbit, 10),
                       'moon': Orbit(moon_orbit, 20),
                       'asteroids': asteroid_orbit_obj})

PARTS.append(orbman)

asteroids_button = Button(text='Go to Asteroid Field', x=-0.75, y=0.15, scale_y=0.08, scale_x=0.25, visible=False, enabled=False)
asteroids_button.on_click = lambda : orbman.goto_orbit('asteroids')
moon_button = Button(text='Go to the Moon', x=-0.75, y=0.05, scale_y=0.08, scale_x=0.25, visible=False, enabled=False)
moon_button.on_click = lambda : orbman.goto_orbit('moon')
sattelite_button = Button(text='Go to Sattelite', x=-0.75, y=-0.05, scale_y=0.08, scale_x=0.25, visible=False, enabled=False)
sattelite_button.on_click = lambda : orbman.goto_orbit('sattelite')
leo_button = Button(text='Go to LEO', x=-0.75, y=-0.15, scale_y=0.08, scale_x=0.25, visible=False, enabled=False)
leo_button.on_click = lambda : orbman.goto_orbit('leo')

oil_tank = PartWrapper(OilTank, scale=0.05, x=0, y=-1.25)
deep_fryer = PartWrapper(DeepFryer, scale=0.5, x=-3.3, y=-0.1)
engine = PartWrapper(Engine, scale=0.5, x=-3.6, y=-3)
battery = PartWrapper(Battery, scale=0.05, x=0, y=0)
watertank = PartWrapper(Watertank, scale=0.09, x=1, y=1)
fridge = PartWrapper(Fridge, scale=0.2, x=2, y=-1.3)
aquarium = PartWrapper(Aquarium, scale=0.3, x=3, y=-3)
penguin = PartWrapper(Penguin, scale=0.8, x=4.2, y=-0.2)

orbman.couple(engine)
engine.couple(battery)
deep_fryer.couple(battery)
asteroid_orbit_obj.set_water_target(watertank)

check_danger_button = Button(text='Check the window ;)', x=-.75, y=.35, scale_y=0.08, scale_x=0.25)
check_danger_button.render = 0
check_danger_button.on_click = lambda : check_window(oil_tank, deep_fryer)

check_danger = DangerManager(check_danger_button)


guy = SpriteSheetAnimation('assets/sprite_sheet_stolen_transparent.png', tileset_size=(4, 4), animations={
    'idle_down': ((0, 3), (0, 3)),
    'walking_down': ((0, 3), (3, 3))
},
                           collider='box',
                           x=-1,
                           y=-1.2)
def run_guy_run():
    guy.play_animation('walking_down')
    deep_fryer._part.holding['food']-=20
guy.on_mouse_enter=lambda : run_guy_run() 
guy.on_mouse_exit=lambda : guy.play_animation('idle_down')

guy.play_animation('idle_down')

title = Text('In espace no one can hear you escream', x=-.5, y=.5, origin=(-.5, .5), scale=2)

## UI Stats
ui_screen = Sprite(
    texture=Texture('assets/computer_stat_screen.png'),
    parent=camera.ui,
    scale_x = 0.135,
    scale_y = 0.134,
    origin=(.5, .5),
    x=window.top_right[0]-0.055,
    y=window.top_right[1],
    z=0.1
)

### Food
stat_text_food = Text(
    'Food: 0',
    x=window.top_right[0]-0.075,
    y=window.top_right[1]-0.04,
    origin=(.5, .5),
)
stat_bar_food = UIProgressBar(
    parent=camera.ui,
    below=stat_text_food,
    max_value=100,
    width_scale=2,
)

### Engine
stat_text_engine = Text(
    'Engine Health: 100',
    x=window.top_right[0]-0.075,
    y=window.top_right[1]-0.09,
    origin=(.5, .5)
)
stat_bar_engine = UIProgressBar(
    below=stat_text_engine,
    parent=camera.ui,
    max_value=100,
    width_scale=2,
    frame_color=color.red
)

### Oil
stat_text_oil = Text(
    'Oil: 0',
    x=window.top_right[0]-0.075,
    y=window.top_right[1]-0.14,
    origin=(.5, .5)
)
stat_bar_oil = UIProgressBar(
    below=stat_text_oil,
    parent=camera.ui,
    max_value=100,
    width_scale=2,
    frame_color=color.white,
    body_color=color.black
)

### Time
stat_text_time = Text(
    'Time: 0',
    x=window.top_right[0]-0.075,
    y=window.top_right[1]-0.19,
    origin=(.5, .5)
)


## Buttons
fryer_button = Button(text='Connect Deep Fryer', x=-0.75, y=0.1, scale_y=0.08, scale_x=0.25)
fryer_button.on_click = lambda : deep_fryer.couple(oil_tank)
engine_button = Button(text='Connect Engine', x=-0.75, y=-0.1, scale_y=0.08,  scale_x=0.25)
engine_button.on_click = lambda : engine.couple(oil_tank)
button_left = Button(text='Look outside', x=-0.75, y=0.45, scale_y=0.08, scale_x=0.25)

view_navigator = ViewNavigator([{'position': 0, 'left text': 'Look outside', 'buttons': [fryer_button, engine_button, check_danger]},
                                {'position': -15, 'left text': 'Go inside', 'buttons': [asteroids_button, moon_button, sattelite_button, leo_button]}], button_left)

dim_overlay = Entity(parent=camera.ui, model='quad', color=color.black, scale=(100, 100), enabled=True, render=20)
dim_overlay.alpha_setter(0)


class Ticker:
    def __init__(self):
        self.val = 0

fryer_ticker = Ticker()


_drag_start = None
_drag_hose = None

# Score is the game logic
score = Score(
    parts={
        'oil_tank': oil_tank,
        'deep_fryer': deep_fryer,
        'engine': engine,
        'battery': battery,
        'watertank': watertank,
        'fridge': fridge,
        'aquarium': aquarium,
        'penguin': penguin,
    },
    background_entity=background,
    orbit_manager=orbman
)

def update():

    if score.is_running:
        fryer_ticker.val += 1
        if fryer_ticker.val % 20 == 0:
            [p.step() for p in PARTS]
            stat_text_food.text = f"Food: {deep_fryer._part.holding['food']}"
            stat_bar_food.set_progress(deep_fryer._part.holding['food'])

            stat_text_engine.text = f"Battery: {int(battery.holding['energy'])}"
            stat_bar_engine.set_progress(battery.holding['energy'])

            stat_text_oil.text = f"Penguin: {int(penguin._part.holding['health'])}"
            stat_bar_oil.set_progress(penguin._part.holding['health'])

            dim_overlay.alpha_setter(0.8 - 0.8 * (battery.holding["energy"]/100))

            stat_text_time.text = f"Time until arrival:\n{score.game_time - int(score.time_elapsed)} space-hours"

            fryer_ticker.val = 0
            check_danger.tick(score.time_elapsed)

            _energy = battery.holding["energy"]
            battery.holding["energy"] = min(_energy + orbman.position / 10, 100)

        if _drag_start:
            _drag_hose.model.vertices = ((_drag_start._sprite.x/15, _drag_start._sprite.y/15, 0), (mouse.x/2, mouse.y/2, 0))
            _drag_hose.model.generate()

    orbman.update()

    asteroid_orbit_obj.update()

    music_man.update(orbman.position)
    score.update()


def input(key):
    global _drag_start
    global _drag_hose
    if key == "left mouse down" and hasattr(mouse.hovered_entity, '_parent') and isinstance(mouse.hovered_entity._parent, PartWrapper):
        _drag_start = mouse.hovered_entity._parent
        x = mouse.start_x
        y = mouse.start_y
        try:
            destroy(_drag_hose)
        except:
            pass
        _drag_hose = Entity(
            position=(0,0),
            scale=15,
            model=Mesh(
                vertices=((x, y, 0), (mouse.x, mouse.y, 0)),
                mode='line',
                thickness=10,
                render_points_in_3d=False
            ),
            color=color.red)

    if key == "left mouse up" and _drag_start:
        destroy(_drag_hose)
        _drag_start.decouple()

        if not hasattr(mouse.hovered_entity, '_parent'):
            return

        drag_stop = mouse.hovered_entity._parent
        if isinstance(drag_stop, PartWrapper) and _drag_start != drag_stop:
            drag_stop.couple(_drag_start)

    if key == "left mouse up":
        _drag_start = None

app.run()
