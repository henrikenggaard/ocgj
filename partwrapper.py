#!/usr/bin/env python3

from ursina import color, destroy, Entity, Mesh, Sprite
from components.progress_bar import ProgressBar
from spaceship import SpaceshipPart

METRIC_COLORS = {"health": color.red, "oil": color.gold, "energy": color.green, "fish": color.salmon, "water": color.blue, "ice": color.cyan}
PARTS = []

class PartWrapper:
    def __init__(self, part, x: float, y: float, scale: float) -> None:
        self._part: SpaceshipPart = part()
        self._sprite = Sprite(texture=self._part.texture, x=x, y=y, scale=scale, collider='box')
        self._sprite._parent = self
        self._sprite.on_click = self._part.fix

        self._output = None

        self._line = None

        self._pbar = ProgressBar(position=(x, y-0.5), origin=(0,0), max_value=100, body_color=METRIC_COLORS.get(self._part.metric, color.green))

        PARTS.append(self)

    def couple(self, other):
        if isinstance(other, PartWrapper):
            self._part.couple(other._part)
            other._output = self
            other.create_line()

        elif isinstance(other, SpaceshipPart):
            self._part.couple(other)

    def decouple(self):
        if self._part.output:
            try:
                self._part.output.inputs.remove(self._part)
            except ValueError:
                pass
            self.output.check_resources()
            self._output = None
            destroy(self._line)

    def step(self):
        self._part.step()
        if self._part.has_all_resources:
            self._sprite.shake(magnitude=100/max(self._part.holding['health'], 1))

        self._pbar.set_progress(self._part.holding[self._part.metric])

    def create_line(self):
        destroy(self._line)
        self._line = Entity(
            position=(0,0),
            model=Mesh(
                vertices=((self._sprite.x,self._sprite.y,0), (self._output._sprite.x,self._output._sprite.y,0)),
                mode='line',
                thickness=10,
                render_points_in_3d=False
            ),
            color=color.red)

    def __getattr__(self, name):
            return getattr(self._part, name)
